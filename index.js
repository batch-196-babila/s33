

// require = import | express was imported
const express = require("express");

// invoke express package to create server/api and saved it in variable which we can refer to later to create routes
const app = express(); 

// express.json() is a method from express that all us to handle the stream of data from our client and receive the data and automatically parse the incoming JSON from the request

// app.use() is a method used to run another function or method for our expressJS API
// used to run middlewares (functions that add features to our application)
app.use(express.json());  

const port = 4000; // port assignment

/* Mock Collection for Courses */

let courses = [
    {
        name: "Phyton 101",
        description: "Learn Python",
        price: 25000,
    },
    {
        name: "ReactJS 101",
        description: "Learn React",
        price: 35000,
    },
    {
        name: "Express 101",
        description: "Learn ExpressJS",
        price: 28000,
    }
];

let users = [
    {
        email: "marybell_knight",
        password: "merrymarybell"
    },
    {
        email: "janedoePriest",
        password: "jobPriest100"
    },
    {
        email: "kimTofu",
        password: "dubuTofu"
    }
]

// used the listen() method of express to assign a port to our server and send message

// creating a route in Express
// access express to have access to its route methods
// Syntax: app.method('/endpoint', (req, res) = >{})

app.get('/', (req, res) => {
    // send() is similar to end() in NodeJS that sends data to message and ends the response
    // Automatically creates and add the headers
    res.send("Hello from our first ExpressJS route");
})

app.post('/', (req, res) => {
    res.send("Hello from our first ExpressJS POST method route");
})

/* Mini Activity */

app.put('/', (req, res) => {
    res.send("Hello from a PUT method route");
})

app.delete('/', (req, res) => {
    res.send("Hello from a DELETE method route");
})


app.get('/courses', (req, res) => {
    res.send(courses);
})

// create route to be able to add new course from a request input

app.post('/courses', (req, res) => {

    // with express.json() the data stream has been captured, the data input has been parsed into a JS object
    console.log(req.body);
    courses.push(req.body);
    // request /req.body contains the body of the request

    //console.log(courses);

    // send the updated users array in the client
    
    res.send(courses);
    console.log(courses);
})

app.listen(port, () => console.log(`Express API running at port 4000`));


/* Activity */

// display users array in the client
app.get('/users', (request, response) => {
    response.send(users);
})

// add new user
app.post('/users', (request, response) => {
    console.log(request.body);
    users.push(request.body);

    response.send(users);
    console.log(users);
});

// stretch goal 1
app.delete('/users', (request, response) => {
    users.pop();
    response.send(users);
})


